package com.lab.lab23.rest;

import com.lab.lab23.model.Faculty;
import com.lab.lab23.model.Mark;
import com.lab.lab23.model.Student;
import com.lab.lab23.service.MarkService;
import com.lab.lab23.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("students")
public class StudentRestController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private MarkService markService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Student>> getAll() {
        List<Student> students = this.studentService.getAll();
        if(students.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Student> doGet(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Student student = studentService.getById(id);
        if(student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Student> doPost(@RequestBody Student student) {
        if(student == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        studentService.save(student);
        return new ResponseEntity<>(student, HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Student> doDelete(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Student student = studentService.getById(id);
        if(student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        studentService.delete(student);
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Student> doPut(@PathVariable Integer id, @RequestBody Student student) {
        if(id == null || student == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(studentService.getById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Student student1 = studentService.put(student, id);
        return new ResponseEntity<>(student1, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}/marks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Mark>> doGetMarks(@PathVariable Integer id) {
        Student student = studentService.getById(id);
        return new ResponseEntity<>(student.getMarkList(), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}/marks/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Mark> doGetMarkByName(@PathVariable Integer id, @PathVariable String name) {
        Student student = studentService.getById(id);
        List<Mark> marks = student.getMarkList();
        if(marks == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Mark mark = marks.stream().filter(mark1 -> mark1.getSubjectName().equals(name)).findFirst().orElse(null);
        if(mark == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(mark, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}/marks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Mark> doPostMark(@PathVariable Integer id, @RequestBody Mark mark) {
        if(mark == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Student student = studentService.getById(id);
        if(student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        markService.save(mark);
        return new ResponseEntity<>(mark, HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}/marks/{idMark}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Mark> doDeleteMark(@PathVariable Integer id, @PathVariable Integer idMark) {
        if(idMark == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Mark mark = markService.getById(idMark);
        if(mark == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        markService.delete(mark);
        return new ResponseEntity<>(mark, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}/marks/{idMark}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Mark> doPutMark(@PathVariable Integer id, @PathVariable Integer idMark, @RequestBody Mark mark) {
        if(idMark == null || mark == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        markService.put(mark, idMark);
        return new ResponseEntity<>(mark, HttpStatus.OK);
    }

}
