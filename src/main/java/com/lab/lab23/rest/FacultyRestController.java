package com.lab.lab23.rest;

import com.lab.lab23.model.Faculty;
import com.lab.lab23.model.Student;
import com.lab.lab23.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("faculties")
public class FacultyRestController {

    @Autowired
    private FacultyService facultyService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Faculty>> getAll() {
        List<Faculty> faculties = this.facultyService.getAll();
        if(faculties.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(faculties, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Faculty> doGet(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Faculty faculty = facultyService.getById(id);
        if(faculty == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(faculty, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Faculty> doPost(@RequestBody Faculty faculty) {
        if(faculty == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        facultyService.save(faculty);
        return new ResponseEntity<>(faculty, HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Faculty> doDelete(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Faculty faculty = facultyService.getById(id);
        if(faculty == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        facultyService.delete(faculty);
        return new ResponseEntity<>(faculty, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Faculty> doPut(@PathVariable Integer id, @RequestBody Faculty faculty) {
        if(id == null || faculty == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(facultyService.getById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Faculty faculty1 = facultyService.put(faculty, id);
        return new ResponseEntity<>(faculty1, HttpStatus.OK);
    }

}
