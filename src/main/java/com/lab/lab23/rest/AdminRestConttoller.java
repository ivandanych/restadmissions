package com.lab.lab23.rest;

import com.lab.lab23.model.Admin;
import com.lab.lab23.model.Information;
import com.lab.lab23.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admins")
public class AdminRestConttoller {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Admin>> getAll() {
        List<Admin> admins = this.adminService.getAll();
        if(admins.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(admins, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Admin> doGet(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Admin admin = adminService.getById(id);
        if(admin == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(admin, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Admin> doPost(@RequestBody Admin admin) {
        if(admin == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        adminService.save(admin);
        return new ResponseEntity<>(admin, HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Admin> doDelete(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Admin admin = adminService.getById(id);
        if(admin == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        adminService.delete(admin);
        return new ResponseEntity<>(admin, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Admin> doPut(@PathVariable Integer id, @RequestBody Admin admin) {
        if(id == null || admin == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(adminService.getById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Admin admin1 = adminService.put(admin, id);
        return new ResponseEntity<>(admin1, HttpStatus.OK);
    }

}
