package com.lab.lab23.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Mark")
public class Mark {

    @Id
    @Column(name = "record_id")
    private Integer id;

    @Column(name = "student_id")
    private Integer studentId;

    @Column(name = "subject")
    private String subjectName;

    @Column(name = "mark")
    private Integer mark;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private Student student;

    public Mark() {
    }

    public void update(Mark mark) {
        if(mark.mark != null){
            this.mark = mark.mark;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }
}
