package com.lab.lab23.service;

import com.lab.lab23.model.Admin;
import com.lab.lab23.model.Information;

import java.util.List;

public interface InformationService {

    Information getById(Integer id);

    void save(Information information);

    void delete(Information information);

    Information put(Information information1, Integer id);

    List<Information> getAll();
}
