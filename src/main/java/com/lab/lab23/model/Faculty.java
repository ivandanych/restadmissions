package com.lab.lab23.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Faculty")
public class Faculty {

    @Id
    @Column(name = "faculty_id")
    private Integer id;

    @Column(name = "faculty_name")
    private String facultyName;

    @Column(name = "count_of_students")
    private Integer countOfStudents;

    public Faculty() {
    }

    public void update(Faculty faculty) {
        if(faculty.facultyName != null){
            facultyName = faculty.facultyName;
        }
        if(faculty.countOfStudents != null){
            countOfStudents = faculty.countOfStudents;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Integer getCountOfStudents() {
        return countOfStudents;
    }

    public void setCountOfStudents(Integer countOfStudents) {
        this.countOfStudents = countOfStudents;
    }
}
