package com.lab.lab23.service;

import com.lab.lab23.model.Mark;
import com.lab.lab23.model.Student;

import java.util.List;

public interface StudentService {

    Student getById(Integer id);

    void save(Student student);

    void delete(Student student);

    Student put(Student student1, Integer id);

    List<Student> getAll();

}
