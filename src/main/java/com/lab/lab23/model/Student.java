package com.lab.lab23.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Student")
public class Student {

    @Id
    @Column(name = "student_id")
    private Integer id;

    @Column(name = "student_name")
    private String name;

    @Column(name = "student_password")
    private String password;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "faculty_id")
    private Integer facultyId;

    @OneToMany(mappedBy = "student")
    private List<Mark> markList;

    public Student() {
    }

    public void update(Student student) {
        if(student.name != null){
            name = student.name;
        }
        if(student.password != null){
            password = student.password;
        }
        if(student.dateOfBirth != null){
            dateOfBirth = student.dateOfBirth;
        }
        if(student.facultyId != null){
            facultyId = student.facultyId;
        }
        if(student.markList != null){
            markList = student.markList;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Integer facultyId) {
        this.facultyId = facultyId;
    }

    public List<Mark> getMarkList() {
        return markList;
    }

    public void setMarkList(List<Mark> markList) {
        this.markList = markList;
    }
}
