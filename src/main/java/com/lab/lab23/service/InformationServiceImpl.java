package com.lab.lab23.service;

import com.lab.lab23.model.Admin;
import com.lab.lab23.model.Information;
import com.lab.lab23.repository.InformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InformationServiceImpl implements InformationService {

    @Autowired
    InformationRepository informationRepository;

    @Override
    public Information getById(Integer id) {
        return informationRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Information information) {
        informationRepository.save(information);
    }

    @Override
    public void delete(Information information) {
        informationRepository.delete(information);
    }

    @Override
    public Information put(Information information1, Integer id) {
        Information information = informationRepository.getOne(id);
        information.update(information1);
        informationRepository.save(information);
        return information;
    }

    @Override
    public List<Information> getAll() {
        return informationRepository.findAll();
    }

}
