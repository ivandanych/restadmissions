package com.lab.lab23.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table
public class Information {

    @Id
    @Column(name = "student_id")
    private Integer studentId;

    @Column(name = "admin_id")
    private Integer adminId;

    @Column(name = "date")
    private Date date;

    public Information() {
    }

    public void update(Information information) {
        if(information.studentId != null){
            studentId = information.studentId;
        }
        if(information.adminId != null){
            adminId = information.adminId;
        }
        if(information.date != null){
            date = information.date;
        }
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date getDate() {
        return date;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public void setDate(Date date) {
        this.date = date;
    }
}
