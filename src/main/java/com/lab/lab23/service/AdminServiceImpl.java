package com.lab.lab23.service;

import com.lab.lab23.model.Admin;
import com.lab.lab23.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminRepository adminRepository;

    @Override
    public Admin getById(Integer id) {
        return adminRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Admin admin) {
        adminRepository.save(admin);
    }

    @Override
    public void delete(Admin admin) {
        adminRepository.delete(admin);
    }

    @Override
    public Admin put(Admin admin1, Integer id) {
        Admin admin = adminRepository.getOne(id);
        admin.update(admin1);
        adminRepository.save(admin);
        return admin;
    }

    @Override
    public List<Admin> getAll() {
        return adminRepository.findAll();
    }

}
