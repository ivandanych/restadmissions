package com.lab.lab23.service;

import com.lab.lab23.model.Admin;

import java.util.List;

public interface AdminService {

    Admin getById(Integer id);

    void save(Admin admin);

    void delete(Admin admin);

    Admin put(Admin admin1, Integer id);

    List<Admin> getAll();


}
