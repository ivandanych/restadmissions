package com.lab.lab23.service;

import com.lab.lab23.model.Information;
import com.lab.lab23.model.Mark;

import java.util.List;

public interface MarkService {

    Mark getById(Integer id);

    void save(Mark mark);

    void delete(Mark mark);

    Mark put(Mark mark1, Integer id);

    List<Mark> getAll();

}
