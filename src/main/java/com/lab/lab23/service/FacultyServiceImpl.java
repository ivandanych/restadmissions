package com.lab.lab23.service;

import com.lab.lab23.model.Admin;
import com.lab.lab23.model.Faculty;
import com.lab.lab23.repository.FacultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyServiceImpl implements FacultyService {

    @Autowired
    FacultyRepository facultyRepository;

    @Override
    public Faculty getById(Integer id) {
        return facultyRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Faculty faculty) {
        facultyRepository.save(faculty);
    }

    @Override
    public void delete(Faculty faculty) {
        facultyRepository.delete(faculty);
    }

    @Override
    public Faculty put(Faculty faculty1, Integer id) {
        Faculty faculty = facultyRepository.getOne(id);
        faculty.update(faculty1);
        facultyRepository.save(faculty);
        return faculty;
    }

    @Override
    public List<Faculty> getAll() {
        return facultyRepository.findAll();
    }


}
