package com.lab.lab23.service;

import com.lab.lab23.model.Mark;
import com.lab.lab23.model.Student;
import com.lab.lab23.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements  StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public Student getById(Integer id) {
        return studentRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Student student) {
        studentRepository.save(student);
    }

    @Override
    public void delete(Student student) {
        studentRepository.delete(student);
    }

    @Override
    public Student put(Student student1, Integer id) {
        Student student = studentRepository.getOne(id);
        student.update(student1);
        studentRepository.save(student);
        return student;
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.findAll();
    }

}
