package com.lab.lab23.service;

import com.lab.lab23.model.Admin;
import com.lab.lab23.model.Faculty;

import java.util.List;

public interface FacultyService {

    Faculty getById(Integer id);

    void save(Faculty faculty);

    void delete(Faculty faculty);

    Faculty put(Faculty faculty1, Integer id);

    List<Faculty> getAll();

}
