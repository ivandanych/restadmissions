package com.lab.lab23.rest;

import com.lab.lab23.model.Faculty;
import com.lab.lab23.model.Information;
import com.lab.lab23.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("informations")
public class InformationRestController {

    @Autowired
    private InformationService informationService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Information>> getAll() {
        List<Information> information = this.informationService.getAll();
        if(information.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(information, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Information> doGet(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Information information = informationService.getById(id);
        if(information == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(information, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Information> doPost(@RequestBody Information information) {
        if(information == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        informationService.save(information);
        return new ResponseEntity<>(information, HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Information> doDelete(@PathVariable Integer id) {
        if(id == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Information information = informationService.getById(id);
        if(information == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        informationService.delete(information);
        return new ResponseEntity<>(information, HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Information> doPut(@PathVariable Integer id, @RequestBody Information information) {
        if(id == null || information == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(informationService.getById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Information information1 = informationService.put(information, id);
        return new ResponseEntity<>(information1, HttpStatus.OK);
    }

}
