package com.lab.lab23.service;

import com.lab.lab23.model.Information;
import com.lab.lab23.model.Mark;
import com.lab.lab23.repository.MarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarkServiceImpl implements MarkService {

    @Autowired
    MarkRepository markRepository;

    @Override
    public Mark getById(Integer id) {
        return markRepository.findById(id).orElse(null);
    }

    @Override
    public void save(Mark mark) {
        markRepository.save(mark);
    }

    @Override
    public void delete(Mark mark) {
        markRepository.delete(mark);
    }

    @Override
    public Mark put(Mark mark1, Integer id) {
        Mark mark = markRepository.getOne(id);
        mark.update(mark1);
        markRepository.save(mark);
        return mark;
    }

    @Override
    public List<Mark> getAll() {
        return markRepository.findAll();
    }

}
